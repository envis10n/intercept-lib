const net = require('net');
const EventEmitter = require('events');

function splitdata(data){
    return data.split('}\n').filter(e=>{
        return e != '';
    }).map(e=>{
        return e+'}';
    });
}

class Intercept extends EventEmitter{
    constructor(){
        super();
        this.uid = null;
        this.token = null;
        this.state = 0;
        this.connected_at = null;
        this.socket;
        this.Connect();
        this.queue = [];
        this.p_queue = 0;
        this.buffer = '';
        this.cfg = null;
        setInterval(()=>{
            if(this.socket != null && this.state == 1 && this.queue.length > 0 && this.p_queue <= Date.now()){
                var d = this.queue.shift();
                var speed = 750;
                if(d.auto && d.data.request == 'command' && d.data.cmd){
                    speed += d.data.cmd.length*50;
                }
                this.p_queue = Date.now()+speed;
                this.socket.write(JSON.stringify(d.data));
            }
        }, 1);
    }
    Queue(obj, auto = false){
        var t = {
            auto: auto,
            data: obj
        };
        this.queue.push(t);
    }
    QueueAuto(obj){
        this.Queue(obj, true);
    }
    SendCommand(cmd){
        this.Queue({request: 'command', cmd: cmd});
    }
    SendCommandAuto(cmd){
        this.QueueAuto({request: 'command', cmd: cmd});
    }
    Emit(event, ...args){
        var t = [event];
        t = t.concat(args);
        this.emit('all', ...t);
        this.emit(event, ...args);
    }
    Connect(){
        this.socket = net.createConnection({
            host: "209.97.136.54",
            port: 13373
        });
        this.socket.on('connect', ()=>{
            this.Emit('connected');
        });
        this.socket.on('error', (err)=>{
            this.Emit('error', err);
        });
        this.socket.on('close', (her)=>{
            this.socket = null;
            this.state = her ? -1 : 0;
            this.uid = null;
            this.connected_at = null;
            this.queue = [];
            this.buffer = '';
            this.p_queue = 0;
            this.cfg = null;
            if(this.state == 0){
                this.Emit('reconnect');
            }
            this.Emit('disconnect', her);
        });
        this.socket.on('data', d=>{
            d = d.toString();
            this.buffer += d;
            if(this.buffer[this.buffer.length-1] == '\n'){
                d = this.buffer;
                this.buffer = '';
            } else return;
            d = splitdata(d);
            d.forEach(data=>{
                this.Emit('data', data);
                try{
                    var dobj = JSON.parse(data);
                    this.Emit('json', dobj);
                } catch(e){
                    this.Emit('badJSON', e);
                }
            });
        });
        this.on('json', (dobj)=>{
            switch(dobj.event){
                case 'info':
                    this.uid = dobj.client_id;
                    this.connected_at = dobj.connected_at;
                    this.state = 1;
                    this.Emit('ready');
                break;
                case 'auth':
                    if(dobj.success){
                        this.token = dobj.token;
                        this.cfg = dobj.cfg;
                        this.Queue({request:"connect", token: dobj.token});
                        this.Emit('authorized');
                    }
                    else
                    {
                        this.Emit('badLogin', dobj.msg);
                    }
                break;
                case 'connect':
                    this.Emit('login', dobj);
                break;
                case 'command':
                    this.Emit(`return_${dobj.cmd}`, dobj.msg);
                break;
            }
        });
        process.on('beforeExit', ()=>{
            this.socket.end();
        });
    }
    Disconnect(){
        if(this.socket != null && this.state == 1){
            this.state = 2;
            this.socket.end();
            return true;
        } else {
            return false;
        }
    }
    Login(obj = {username: '', password: ''}){
        if(this.state != 1) return false;
        this.Queue({request: 'auth', login: obj});
        return true;
    }
    Register(obj = {username: '', password: ''}){
        if(this.state != 1) return false;
        this.Queue({request: 'auth', register: obj});
        return true;
    }
}

module.exports = Intercept;