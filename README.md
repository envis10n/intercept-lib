# intercept-lib

Intercept wrapper and library for Node.js
___
*NOTE: Intercept is an online game. This is simply a library for connecting to the game from a Node.js application.*
[Get Intercept on Itch.io](https://bubmet.itch.io/intercept)
#### Getting Started
___
```js
const Intercept = require('intercept-lib'); // Require the module

var intercept = new Intercept(); // Create an instance of the module's exported class.

// Listen for the 'ready' event to know when the connection process has completed.
intercept.on('ready', ()=>{
    // Connected and ready. 
});
```

#### Usage
___
```js
const Intercept = require('intercept-lib');

var intercept = new Intercept();

intercept.on('ready', ()=>{
    intercept.on('any', (...args)=>{
        var event = args[0];
        args = args.slice(1);
        console.log(`Event: ${event}`, ...args); // Log the event name and any arguments passed for all events emitted by the instance.
    });
    intercept.on('json', dobj=>{
        // Emitted when valid JSON data is ready to be handled.
        /* Events List
            chat - {msg: String}
            broadcast - {msg: String}
            info - {connected_at: Number, client_id: String, client_type: String, date: Number}
            auth - {success: Boolean, token: String, cfg: Object}
            connect - {success: Boolean, msg: String, cfg: Object}
            connected - {player: {ip: String, conn: String}}
            command - {cmd: String, msg: String}
            cfg - {cfg: Object}
            error - {error: String}
        */
        console.log(dobj);
    });
    intercept.on('login', dobj=>{
        // Emitted after successfully logging in.
        //intercept.SendCommand('cmds'); Send command 'cmds' as if it were typed into the terminal.
        // Anything done with automation in mind should be sent using SendCommandAuto
        // intercept.SendCommandAuto('cmds');
    });
    intercept.on('badLogin', err=>{
        // Authentication failed.
        console.log(err);
    });
    // Log our user in.
    intercept.Login({
        username: 'username',
        password: 'password'
    });
});
```